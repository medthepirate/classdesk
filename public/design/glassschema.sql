

show tables;

create table students (
	studentId int,
    studentName varchar(255),
    schoolId int,
    grade int,
    userId int,
    phoneNumber varchar(255),
	paymentType int,
    paymentStatus int
    );
    
create table instructors (
	instructorId int,
    instructorName varchar(255),
    schoolId int,
    grade int,
    userId int,
    phoneNumber varchar(255)
    );

create table glassUsers (
	userType int,
    schoolId int,
    userId int,
    phoneNumber varchar(255),
    userName varchar(255),
    pw varchar(255),
    guid varchar(255)
    );
    

create table glassUserTypes (
	schoolId int,
	userTypeId int,
    userType varchar(255)
    );
insert into glassUserTypes(schoolId, userTypeId, userType)
values(0, 1, 'super');
insert into glassUserTypes(schoolId, userTypeId, userType)
values(0, 2, 'owner');
insert into glassUserTypes(schoolId, userTypeId, userType)
values(0, 3, 'instructor');
insert into glassUserTypes(schoolId, userTypeId, userType)
values(0, 4, 'student');
 
 create table sessions (
	schoolId int,
	sessionId int,
    sessionDate date,
    sessionNumber int,	
    sessionSyllabus int,
    sessionLesson int,
    sessionInstructor int,
    sessionStudentList varchar(255),
    );
create table tests(
    sessionId int,
    testId int,
    testName varchar(255),
    schoolId int,
    instructorId int,
    studentId int,
    syllabusId int
);
create table testItem(
    testItemId int,
    testId varchar(255),
    testItemName varchar(255),
    testItemValue varchar(255)
);
    
     create table syllabus (
	syllabusId int,
    syllabusDate date,
    schoolId int,
	syllabusName varchar(255)
    );
 create table syllabusItems (
	syllabusId int,
    syllabusItemText date,
    syllabusItemPosition int	
    
    );
 create table lessons (
	syllabusId int,
    lessonId int,
    schoolId int,	
    lessonName varchar(255)
    );
 create table lessonItems (
	lessonId int,
    lessonItemText date,
    
    lessonItemPosition int	
    
    );
 create table customLists (
	customListId int,
    customListName varchar(255),
    schoolId int	
    
    );
 create table customListItems (
	customListId int,
    customListItemText varchar(255),
    customListItemPosition int	
    
    );

create table schools(
	schoolId int,
	paymentStatusId int,
	schoolName varchar(255),
	schoolContactId int
	
);
create table paymentTypes(
	paymentTypeId int,
	paymentTypeName varchar(255)
);
create table paymentStatus(
	paymentStatusId int,
	paymentStatusName varchar(255)
);
insert into paymentStatus(paymentStatusId, paymentStatusName)
values(1, 'up to date');

insert into paymentStatus(paymentStatusId, paymentStatusName)
values(2, 'due');

insert into paymentStatus(paymentStatusId, paymentStatusName)
values(3, 'overdue');

insert into paymentStatus(paymentStatusId, paymentStatusName)
values(4, 'withdraw service');

insert into paymentTypes(paymentTypeId, paymentTypeName)
values(1, 'cash on the door');
insert into paymentTypes(paymentTypeId, paymentTypeName)
values(2, 'cash bulk');
insert into paymentTypes(paymentTypeId, paymentTypeName)
values(3, 'debit monthly');
insert into paymentTypes(paymentTypeId, paymentTypeName)
values(4, 'other');


