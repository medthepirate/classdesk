const getSchool = () => {
    if (localStorage.getItem("school") != null && localStorage.getItem("school") != undefined) {
        _school = JSON.parse(localStorage.getItem("school"));
        $("#school-name").text(_school.schoolName);
    } else {
        $("#school-name").text('name your school');

    }
}

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}

const removeLocalStorageItem = (callback, item) => {
    localStorage.removeItem(item);
    callback();
}

const setCookie = (cname, cvalue, exdays = 365) => {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

const getCookie = (cname) => {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}