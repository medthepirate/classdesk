
//demo
var user = new user(2, 1, 1,000000000,'med','glass');
var school = new school(1, 'testschool', 1);
var page = "index";
//rollcall
var _rollCall = [];
var _student = {};
var nameLineMarkup = "<p class='name-line'>NAMETEXT</p>";
var students = [];
//studentId,userId,schoolId,grade,studentName,phoneNumber,paymentType,paymentStatus
students.push(
    new student(1,2,1,3,'joe studentone', 01234567, 1, 1)
);
students.push(
    new student(2,3,1,3,'joe studenttwo', 01234567, 1, 1)
);
students.push(
    new student(3,4,1,3,'joe studentthree', 01234567, 1, 1)
);
students.push(
    new student(4,5,1,3,'joe studentfour', 01234567, 1, 1)
);
students.push(
    new student(5,6,1,8,'joe studentfive', 01234567, 1, 1)
);
students.push(
    new student(6,7,1,5,'joe studentsix', 01234567, 1, 1)
);
students.push(
    new student(7,8,1,2,'joe studentseven', 01234567, 1, 1)
);
students.push(
    new student(8,9,1,9,'joe studentoneeight', 01234567, 1, 1)
);
students.push(
    new student(9,10,1,6,'joe studentnine', 01234567, 1, 1)
);
//schoolId, sessionDate, sessionNumber,sessionSyllabus,sessionInstructor,sessionStudentList,sessionTests
var lessons = [];
var d =  new Date("dd-MM-yyyy");
d = Date.now
lessons.push(
    new session(1,d,1,1,1,0,0)
);

function addFunctionNewLine(){
    switch(page){
        case "rollcall":
            $('.name-line').addClass('rollcall-line');
            break;
        case "managestudents":
            $('.name-line').addClass('managestudents-line');
            break;
        default:
            break;
    }
    if($('.rollcall-line') != undefined){
        $('.rollcall-line').click(function(){
            for(var i=0;i<students.length;i++){
                var inputValue = new RegExp($(this).text().toLowerCase(),"g");
                var _match = students[i].studentName.toLowerCase().trim().match(inputValue);
                if(_match != null && _match != undefined){
                    if(_match.length>0){
                        _student = students[i];
                        break;
                    }
                }
            } 
            $('.name-line').remove();
            _rollCall.push(_student);
            console.log(_student.studentName + " added to roll call");
        });
    }
    if($('.managestudents-line') != undefined){
        $('.managestudents-line').click(function(){
            for(var i=0;i<students.length;i++){
                var inputValue = new RegExp($(this).text().toLowerCase(),"g");
                var _match = students[i].studentName.toLowerCase().trim().match(inputValue);
                if(_match != null && _match != undefined){
                    if(_match.length>0){
                        _student = students[i];
                        break;
                    }
                }
            } 
            $('.name-line').remove();
            _rollCall.push(_student);
            console.log(_student.studentName + " selected");
            $('.userScreen').toggleClass('hidden');
        });
    }
}
$(function(){
    $('#uname').text(user.userName);
    $('#startRoll').click(function(){
        $('.userScreen').toggleClass('hidden');
    });
    $('#nameInput').keyup(function(e){
        
        $('.name-line').remove();
        if(students!=undefined){
           for(var i=0;i<students.length;i++){
                var inputValue = new RegExp($(this).val().toLowerCase(),"g");
                var _match = students[i].studentName.toLowerCase().trim().match(inputValue);
               if(_match != null && _match != undefined){
                    if(_match.length>0){
                        $('#nameBin').append(nameLineMarkup.replace(/NAMETEXT/g, students[i].studentName))
                    }
                }
            } 
        }
        
        addFunctionNewLine();
    });
    
});

