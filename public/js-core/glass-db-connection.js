
function guid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

const typeme = () => {
    return 'xxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

function createUser(userName, password, email, type, confirmpassword) {
    if (userName == null || userName == undefined || !(userName.length > 0)) return { success: false, reason: 'username' };
    if (password == null || password == undefined || !(password.length > 0)) return { success: false, reason: 'password' };
    if (email == null || email == undefined || !(email.length > 0)) return { success: false, reason: 'email' };
    if ((confirmpassword == null || confirmpassword == undefined || !(confirmpassword.length > 0)) || !(password === confirmpassword)) return { success: false, reason: 'confirmPassword' };
    let d = new Date(Date.now());
    let _d = new Date(Date.now());
    _d.setDate(d.getDate() + parseInt(-20));
    let n = d.toDateString();
    let m = _d.toDateString();
    $.post(`${user_endpoint}`, JSON.stringify({
        user_name: userName,
        password: password,
        email: email,
        guid: guid(),
        last_paid: m,
        signed_up: n,
        payment_status: status_payment_trial,
        phone_number: "",
        serialised_school: "",
        user_type: type,
        active: "true"
    }), (data, status) => {
        console.log(status);
        if (status === 'success') {
            let conf = confirm(`thanks ${userName}, you will now be redirected to the homepage to log in`)
            if (conf) window.location.replace('index.html');
        } else {
            // TODO: log error
            // if(data.Items[0].error === error_user_exists) alert(`${userName} already exists in classDesk, please choose another user name`);

            console.dir(data)
        }
        return { success: status, reason: data }
    });
}

const logInSuper = (username, password) => {
    $.get(`${super_endpoint}?username=${username}&password=${password}`, function (data, status) {
        try {
            body = data.Items[0];
        } catch (e) {
            // resp = JSON.parse("{ \"notes\": \"no user found\" }");
        }
    });
}

function authenticateUser(userName, password) {
    let body = {};
    $.get(`${user_endpoint}?username=${userName}&password=${password}`, function (data, status) {
        try {
            body = data.Items[0];
        } catch (e) {
            resp = JSON.parse("{ \"notes\": \"no user found\" }");
        }

        if (body.error == null) {
            console.log(`it worked. the user is: ${body.user_name}`);
            if (body.active == 'false') {
                alert('that user is deactivated, please contact classDesk');
                return;
            }
            checkStatus(body.user_name, new Date(Date.parse(body.last_paid)));
            _user = new user(body.user_type, body.phone_number, body.user_name, body.guid, body.last_paid, body.payment_status, body.email);
            switch (body.user_type) {
                case usertype_school:
                    // set school from db
                    _school = JSON.parse(body.serialised_school);
                    saveSchool();

                    break;
                case usertype_student:
                case usertype_super:
                    logInSuper(username, password);
                default:
                    break;
            }

        } else {
            switch (body.error) {
                case error_payment:
                    window.location.replace('payment.html');
                    break;
                case error_invalid_details:
                case error_incorrect_password:
                case error_deactivated_user:
                default:
                    alert(`it didn't work: ${body.error}`);
                    break;
            }
        }

        readyState = true;
    });

}

function getSchoolFromDB(u, password) {
    authenticateUser(u.userName, password);
    loggedInFunctions();
}

function setSchool(school, u) {
    $.ajax({
        url: `${user_endpoint}`,
        type: 'PUT',
        data: JSON.stringify({ "fieldToUpdate": "serialised_school", "value": school, "user_name": u.userName, "guid": u.guid }),
        contentType: 'application/json',
        success: function (jsonData) {
            console.dir(jsonData);
        },
        error: function (e) {
            console.dir(e);
        },
        processData: false,
        dataType: 'json',
        crossDomain: true
    });
}

function setStatus(username, status) {
    $.ajax({
        url: `${user_endpoint}`,
        type: 'PUT',
        data: JSON.stringify({ "fieldToUpdate": "payment_status", "value": status, "user_name": username }),
        contentType: 'application/json',
        success: function (jsonData) {
            console.dir(jsonData);
        },
        error: function (e) {
            console.dir(e);
        },
        processData: false,
        dataType: 'json',
        crossDomain: true
    });
}

function setStatusRed(username) {
    setStatus(username, status_payment_due);
}

function setStatusGreen(username){
    setStatus(username, status_payment_good_standing);
}