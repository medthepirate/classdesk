const user_endpoint = 'https://bgd5k2n8lg.execute-api.us-east-2.amazonaws.com/default/glass_getUser';
const ENDPOINT_CRM_USER = 'https://ditkzjf2n2.execute-api.us-east-2.amazonaws.com/default/friendlyCRMUser';
const ENDPOINT_ITEM = 'https://1jyolpz419.execute-api.us-east-2.amazonaws.com/default/friendlyCRMElements';

const super_endpoint = '';
const logging_endpoint = '';
const status_payment_due = 'red';
const status_payment_trial = 'amber';
const status_payment_good_standing = 'green';
// errors
const error_invalid_details = 'invalid details';
const error_incorrect_password = 'password';
const error_deactivated_user = 'deactivated';
const error_payment = 'payment';
const error_user_exists = "Cannot set property 'Items' of undefined";

// usertypes

const usertype_super = 'super';
const usertype_school = 'school';
const usertype_student = 'student';

// elements

const element_class_students = ``;
const element_class_students_panel = ``;
const element_horizontal_rule = `<div class='row'><hr/></div>`
const list_group_open = `<div class='list-group glass-list-group'>`;
const list_group_close = `</div>`;

const lesson_panel_id = 'mainmenu-4';
const student_panel_id = 'mainmenu-2';


