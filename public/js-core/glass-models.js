function student(studentName, contact)
{
    var thisStudent = this;
    thisStudent.grade = "no grade";
    thisStudent.studentName = studentName;
    thisStudent.contact = contact;
    thisStudent.paymentStatus = "no payment";
}
function lessonPlan(lessonName){
    thisLesson = this;
    thisLesson.lessonItems = [];
    thisLesson.lessonName = lessonName;
    thisLesson.datesTaught = [];
}
function lessonItem(itemName, itemNotes){
    thisLessonItem = this;
    thisLessonItem.itemName = itemName;
    thisLessonItem.itemNotes = itemNotes;
}
function user(userType,phoneNumber,userName, guid, lastPaid, paymentStatus, email){
    thisUser = this;
    thisUser.email = email
    thisUser.userType = userType;
    thisUser.phoneNumber = phoneNumber;
    thisUser.userName = userName;
    thisUser.guid = guid;
    thisUser.lastPaid = lastPaid;
    thisUser.paymentStatus = paymentStatus;
    
}
function instructor(instructorId,instructorName,schoolId,grade,userId,phoneNumber){
    thisInstructor = this;
    thisInstructor.instructorId = instructorId;
    thisInstructor.instructorName = instructorName;
    thisInstructor.schoolId = schoolId;
    thisInstructor.grade = grade;
    thisInstructor.userId = userId;
    thisInstructor.phoneNumber = phoneNumber
}
function syllabus(syllabusName){
    thisSyllabus = this;
    thisSyllabus.syllabusDate = new Date().toString("dd-mm-yyyy");
    thisSyllabus.syllabusName = syllabusName;
    thisSyllabus.syllabusItems = [];

}
function syllabusItem(title, notes){
    thisSyllabusItem = this;
    thisSyllabusItem.title = title;
    thisSyllabusItem.notes = notes;
}
function sessionInstance(){
    thisSession = this;
    thisSession.sessionDate = "";
    thisSession.sessionNumber = 0;
    thisSession.sessionSyllabus = new syllabus("placeholderName");
    thisSession.sessionInstructor = "";
    thisSession.classStudents = [];
    thisSession.sessionTests = [];
    thisSession.LessonPlan = new lessonPlan('placeholderName');
}
function school(schoolName, owner){
    var thisSchool = this;
    thisSchool.schoolName = schoolName,
    thisSchool.students = [],
    thisSchool.owner = owner,
    thisSchool.syllabii = [],
    thisSchool.sessions = [],
    thisSchool.instructors = [],
    thisSchool.lessonPlans = []
}

function test(){

}
function testItem(){

}
function customList(){

}

// exports.student = function(studentId){
//     con.connect(function(err) {
//     if (err) throw err;
//     con.query("SELECT * FROM students WHERE studentId =" + studentId, function (err, result, fields) {
//         if (err) throw err;
//         console.log(result);
//         return result;
//   });
// });    
// }
