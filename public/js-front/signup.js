const _typeme = typeme();
$('#typeme').attr('placeholder', _typeme);
const signupButton_click = () => {
    if(_typeme != $('#typeme').val()) {
        $('#typeme').css('border', '1px solid red');
        return;
    }
    $('input').css('border', '2px hidden transparent');
    const email = $('#email').val();
    const username = $('#username').val();
    const password = $('#password').val();
    const confirmpassword = $('#confirmPassword').val();
    const type = "school";
    
    let result = createUser(username, password, email, type, confirmpassword);
    if(result.success){
        alert('user created please go to the homepage and sign in');
    }else{
        //alert(`user creation failed, reason: ${result.reason}`);
        $(`#${result.reason}`).css('border', '1px solid red');
    }
}