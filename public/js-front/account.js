let storage = true;
let partiallogin = false;
let loggedInUser = null;

const reload = () => {
    window.location.replace('payment.html');
}
const daysLeftInTrial = (lastPaid) =>{
    let lastpaid = new Date(lastPaid);
    let endOfTrial = lastpaid.setDate(lastpaid.getDate() + parseInt(28));
    let now = new Date(Date.now());
    let result = Math.abs(endOfTrial - now) / 1000;
    let days = Math.floor(result / 86400);

    return days;
}
const checkStatus = (username, lastpaid) => {
    let now = new Date(Date.now());
    let result = Math.abs(now - lastpaid) / 1000;
    let days = Math.floor(result / 86400);
    let month = lastpaid.getMonth();
    let year = lastpaid.getFullYear();
    let threshold = daysInMonth(month, year);
    if (days > threshold) {
        setStatusRed(username);
        logout(true);
    }
}
const getParameterByName = (name, url) => {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

let logout = (forced) => {
    if (!forced) {
        let conf = confirm("log out?");
        if (conf) {
            removeLocalStorageItem(window.location.reload(), 'loggedInUser');
        }
    } else {
        removeLocalStorageItem(window.location.replace('payment.html'), 'loggedInUser');
    }
}

const setLoggedInUser = () => {

    loggedInUser = _user;
    localStorage.setItem("loggedInUser", JSON.stringify(loggedInUser));
}
if (typeof (Storage) !== "undefined") {
    // is there a logged in user?
    if (getParameterByName("nomatch") == "true") {
        localStorage.removeItem("loggedInUser");
        
    }
    if (window.localStorage.getItem("loggedInUser") != null && localStorage.getItem("loggedInUser")[0] == "{") {

        partiallogin = true;

        loggedInUser = JSON.parse(localStorage.getItem("loggedInUser"));
        
        if (loggedInUser.userName == null || loggedInUser.userName == undefined) {
            localStorage.removeItem("loggedInUser");
            
            location.reload();
        }
        checkStatus(loggedInUser.userName, new Date(Date.parse(loggedInUser.lastPaid)));
    }
    //else login
    else {
        function authenticateUserWrapper(username, password) {
            authenticateUser(username, password);
            
        }
    }
} else {
   storage = false;
   // Sorry! No Web Storage support..
}
