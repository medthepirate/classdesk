
let actionCount = 0;
const actionCountThreshold = 10;

//elements
const el_spinner = $('#spinner');
const el_filter_classes = $('#filterClasses');
const el_notification = $('.classdesk-notification');
const el_schoolname = $("#school-name");
const el_trialtext = $('.trial');
const el_classstage = $('#class-stage');
// account
const el_username = $('#uname');
const el_logout = $('#logout');
const el_backup = $('#backup');
const el_load = $('#load');
const el_back = $('.back');
//containers
const el_mainmenu = $('#mainMenu');
const el_appsection = $('.app-section');
// functional elements
const el_add_class_student = $('#addClassStudent');
// dynamically populated elements
const el_class_lesson_input_list = $('#class-lesson-input-list');
// add class
const el_class_date = $('#classDate');
const el_class_instructor = $('#classInstructor');
const el_class_student_list = $('#class-student-list');
const el_class_lesson_list = $('#class-lesson-list');
// classes 
const el_classlist = $('#current-class-list');
const el_lesson_taught = $('#lessonTaught');
// student
const el_studentlist = $('#current-student-list');
// lessons
const el_lessonlist = $('#current-lessonplan-list');

const resetClassFilter = () => {
    populateClassesList();
}

const setTrialText = (days) => {
    el_trialtext.html(`enjoy your trial for ${days} more days`);
}

const replaceAll = (inputString, stringToReplace, stringToReplaceWith) => {
    return inputString.split(stringToReplace).join(stringToReplaceWith);
}

const openLessonFromClass = (e) => {
    console.dir(e);
    let lessonName = e.text;
    $('.modal-backdrop').hide(0);
    el_back.click();
    $(`#${lesson_panel_id}`).click();
    $(`.${replaceAll(lessonName, ' ', '_')}`).click();
}
const openStudentFromClass = (e) => {
    console.dir(e);
    let studentName = e.text;
    $('.modal-backdrop').fadeOut();
    el_back.click();
    $(`#${student_panel_id}`).click();
    $(`.${replaceAll(studentName, ' ', '_')}`).click();
}
function saveSchool() {
    localStorage.setItem("school", JSON.stringify(_school));
}
function backupSchool() {
    setSchool(JSON.stringify(_school), loggedInUser);
}
function loadSchool() {
    var conf = prompt("retrieve the saved school? this will overwrite the one currently in local storage..", "type your password here");
    if (conf != null) {
        getSchoolFromDB(loggedInUser, conf);
    } else {

    }
}
function startClass() {
    el_classstage.toggleClass('hidden');
}

// modals
const populateClassesList = (sessions = _school.sessions) => {
    var _html = ``;
    if (sessions.length > 0) {
        _html = list_group_open;
        for (var i = 0; i < sessions.length; i++) {

            var lessonName = "no lesson recorded";
            if (sessions[i].lessonPlan != null) {
                lessonName = _school.sessions[i].lessonPlan.lessonName.toString();
            }

            var classStudentsHtml = ``;
            if (sessions[i].classStudents.length > 0) {
                for (var k = 0; k < sessions[i].classStudents.length; k++) {
                    classStudentsHtml +=
                        `<div class='row'><hr/></div>
                    <div class='form-group'>
                        <p> student name: <a onclick='openStudentFromClass(this)' id='lesson-item-name-${k}-${i}'>${sessions[i].classStudents[k].studentName}</a>
                    </div>
                    <div class='form-group'>
                        <p>grade: ${sessions[i].classStudents[k].grade}</p>
                    </div>
                    <div class='btn-group'>
                        <button type='button' class='btn btn-danger' id='deleteclassstudent-${i}-${k}' data-dismiss='modal' onclick='deleteClassStudent(this)'>delete item</button>
                    </div>`;
                }
            }

            _html +=
                `       
                <a class='list-group-item' id='class-instance-${i}' data-toggle='modal' data-target='#classModal${i}'>
                        ${sessions[i].sessionDate.toString()}-${sessions[i].instructor}
                    <button class='btn btn-danger pull-right' id='class-${i}' onclick='deleteClass(this)'>delete lesson</button>
                </a>
                <div id='classModal${i}' class='modal fade' role='dialog'>
                    <div class='modal-dialog'>
                        <div class='modal-content'>
                            <div class='modal-header'>
                                <button type='button' class='close' data-dismiss='modal'>&times;</button>
                                <h4 class='modal-title'>class details</h4>
                            </div>
                            <div class='modal-body'>
                                <div class='form-group'>
                                    <input class='form-control class-field' type='text' id='className-${i}' value='${_school.sessions[i].sessionDate.toString()}' placeholder='lesson name'/>
                                </div> 
                                <div class='form-group'>
                                    <p>
                                        date of class: ${sessions[i].sessionDate.toString()}
                                    </p>
                                </div>
                                <div class='form-group'>
                                    <p>
                                        instructor of class: ${sessions[i].instructor}
                                    </p>
                                </div>
                                <div class='form-group'>
                                    <p>
                                        <a onclick='openLessonFromClass(this)'>${lessonName}</a>
                                    </p>
                                </div>
                                <div class='studentlist'>
                                    <div class='row'><hr/></div>
                                        <h5>students</h5>
                                        ${classStudentsHtml}
                                </div>
                            </div>
                            <div class='modal-footer'>
                                <div class='btn-group'>
                                    <button type='button' id='save-class-${i}' data-dismiss='modal' onclick='saveClass(this)' class='btn btn-success'>save</button>
                                    <button type='button' class='btn btn-danger' data-dismiss='modal'>close</button>
                                </div>
                            </div>
                        </div> 
                    </div> 
                </div>
                `;
        }
        _html += list_group_close;
    } else {
        _html = "<div class='alert warning'><p>no lesson plans in this list</p></div>";
    }
    el_classlist.html(_html);
}

const populateLessonPlanList = () => {
    var _html = ``;
    if (_school.lessonPlans.length > 0) {

        _html = list_group_open;

        for (var i = 0; i < _school.lessonPlans.length; i++) {
            var lessonItemsHtml = ``;
            if (_school.lessonPlans[i].lessonItems.length > 0) {
                for (var k = 0; k < _school.lessonPlans[i].lessonItems.length; k++) {
                    lessonItemsHtml +=
                        `${element_horizontal_rule}
                        <div class='form-group'>
                            <input type='text' class='form-control' id='lesson-item-name-${k}-${i}' placeholder='technique/item name' value='${_school.lessonPlans[i].lessonItems[k].itemName}'/>
                        </div>
                        <div class='form-group'>
                            <input type='text' class='form-control' id='lesson-item-notes-${k}-${i}' placeholder='technique/item notes' value='${_school.lessonPlans[i].lessonItems[k].itemNotes}'/>
                        </div>
                        <div class='btn-group'>
                            <button type='button' class='btn btn-success' id='editlessonitem-${i}-${k}' data-dismiss='modal' onclick='saveLessonItem(this)'>save item</button>
                            <button type='button' class='btn btn-danger' id='deletelessonitem-${i}-${k}' data-dismiss='modal' onclick='deleteLessonItem(this)'>delete item</button>
                        </div>`
                        ;

                }

            }

            _html +=
                `<a class='list-group-item ${replaceAll(_school.lessonPlans[i].lessonName, ' ', '_')}' id='lesson-instance-${i}' data-toggle='modal' data-target='#lessonModal${i}'>
                    ${_school.lessonPlans[i].lessonName} 
                    <button class='btn btn-danger pull-right' id='lesson-${i}' onclick='deleteLessonPlan(this)'>delete lesson</button>
                </a>
                <div id='lessonModal${i}' class='modal fade' role='dialog'>
                    <div class='modal-dialog'>
                        <div class='modal-content'>
                            <div class='modal-header'>
                                <button type='button' class='close' data-dismiss='modal'>&times;</button>
                                <h4 class='modal-title'>lesson details</h4>
                            </div>    
                            <div class='modal-body'>
                                <div class='form-group'>
                                    <input class='form-control lesson-field' type='text' id='lessonPlanName-${i}' value='${_school.lessonPlans[i].lessonName}' placeholder='lesson name'/>
                                </div> 
                                ${element_horizontal_rule}
                                <h5>add a lesson item with these inputs</h5>
                                <div class='form-group'>
                                    <input type='text' class='form-control lesson-field' id='lesson-item-name-${i}' placeholder='technique/item name' />
                                </div>
                                <div class='form-group'>
                                    <input type='text' class='form-control lesson-field' id='lesson-item-notes-${i}' placeholder='technique/item notes' />
                                </div>
                                <div class='form-group'>
                                    <button class='btn btn-info add-lesson-item' id='addlessonitem-${i}' onclick='addLessonItem(this)' data-dismiss='modal'>add lesson item</button>
                                </div>
                                ${element_horizontal_rule}
                                <h5>saved lesson items</h5>
                                ${lessonItemsHtml}
                            </div>
                            <div class='modal-footer'>
                                <div class='btn-group'>
                                    <button type='button' id='save-lesson-${i}' data-dismiss='modal' onclick='saveLessonPlan(this)' class='btn btn-success'>save</button>
                                    <button type='button' class='btn btn-danger' data-dismiss='modal'>close</button>
                                </div>
                            </div>
                        </div> 
                    </div> 
                </div>`
                ;

        }
        _html += list_group_close;
    } else {
        _html = "<div class='alert warning'><p>no lesson plans in this list</p></div>";
    }
    el_lessonlist.html(_html);
}

// const populateSyllabusList = () => {
//     var _html = ``;
//     if (_school.syllabii.length > 0) {
//         var cnt = 0;
//         _html = list_group_open;
//         for (var i = 0; i < _school.syllabii.length; i++) {
//             var syllabusItemsHtml = "";
//             if (_school.syllabii[i].syllabusItems.length > 0) {
//                 for (var k = 0; k < _school.syllabii[i].syllabusItems.length; k++) {
//                     syllabusItemsHtml +=
//                         `<div class='row'><hr/></div>
//                         <div class='form-group'>
//                             <input type='text' class='form-control' id='syllabus-item-name-${k}-${i}' placeholder='technique/item name' value='${_school.syllabii[i].syllabusItems[k].title}'/>
//                         </div>
//                         <div class='form-group'>
//                             <input type='text' class='form-control' id='syllabus-item-notes-${k}-${i}' placeholder='technique/item notes' value='${_school.syllabii[i].syllabusItems[k].notes}'/>
//                         </div>
//                         <div class='btn-group'>
//                             <button type='button' class='btn btn-success' id='editsyllabusitem-${i}-${k}' data-dismiss='modal' onclick='saveSyllabusItem(this)'>save item</button>
//                             <button type='button' class='btn btn-danger' id='deletesyllabusitem-${i}-${k}' data-dismiss='modal' onclick='deleteSyllabusItem(this)'>delete item</button>
//                         </div>`
//                         ;
//                     cnt++;
//                 }

//             }

//             _html += 
//                 `<a class='list-group-item' id='syllabus-instance-${i}' data-toggle='modal' data-target='#syllabusModal${i}'>
//                     ${_school.syllabii[i].syllabusName}<button class='btn btn-danger pull-right' id='syllabus-${i}' onclick='deleteSyllabus(this)'>delete syllabus</button></a>
//                     <div id='syllabusModal${i}' class='modal fade' role='dialog'>
//                         <div class='modal-dialog'>
//                             <div class='modal-content'>
//                                 <div class='modal-header'>
//                                     <button type='button' class='close' data-dismiss='modal'>&times;</button>
//                                     <h4 class='modal-title'>syllabus details</h4>
//                                 </div>    
//                                 <div class='modal-body'>
//                                     <div class='form-group'>
//                                         <input class='form-control syllabus-field' type='text' id='syllabusName-${i}' value='${_school.syllabii[i].syllabusName}' placeholder='student name'/>
//                                     </div> 
//                                     <div class='form-group'>
//                                         <input type='text' class='form-control syllabus-field' id='syllabus-item-name-add-${i}' placeholder='technique/item name' />
//                                     </div>
//                                     <div class='form-group'>
//                                         <input type='text' class='form-control syllabus-field' id='syllabus-item-notes-add-${i}' placeholder='technique/item notes' />
//                                     </div>
//                                     <div class='form-group'>
//                                         <button class='btn btn-info add-syllabus-item' id='addsyllabusitem-${i}' onclick='addSyllabusItem(this)' data-dismiss='modal'>add syllabus item</button>
//                                     </div>
//                                     ${syllabusItemsHtml}
//                                 </div>
//                                 <div class='modal-footer'>
//                                     <div class='btn-group'>
//                                         <button type='button' id='save-syllabus-${i}' data-dismiss='modal' onclick='saveSyllabus(this)' class='btn btn-success'>save</button>
//                                         <button type='button' class='btn btn-danger' data-dismiss='modal'>close</button>
//                                     </div>
//                                 </div>
//                             </div> 
//                         </div> 
//                     </div>`
//                     ;
//         }

//         _html += list_group_close;
//     } else {
//         _html = `<div class='alert warning'><p>no syllabii in this list</p></div>`;
//     }
//     $('#current-syllabus-list').html(_html);
// }

const populateStudentList = () => {
    var _html = ``;
    if (_school.students.length > 0) {
        _html = list_group_open;

        for (var i = 0; i < _school.students.length; i++) {
            _html +=
                `<a class='list-group-item ${replaceAll(_school.students[i].studentName, ' ', '_')}' id='${i}' data-toggle='modal' data-target='#studentmodal${i}'>
                ${_school.students[i].studentName}<button class='btn btn-danger pull-right' id='student-${i}' onclick='deleteStudent(this)'>delete student</button></a>
            <div id='studentmodal${i}' class='modal fade' role='dialog'>
                <div class='modal-dialog'>
                    <div class='modal-content'>
                        <div class='modal-header'>
                            <button type='button' class='close' data-dismiss='modal'>&times;</button>
                            <h4 class='modal-title'>student details</h4>
                        </div>
                        <div class='modal-body'>
                            <div class='form-group'>
                                <input class='form-control student-field' type='text' id='studentName-${i}' value='${_school.students[i].studentName}' placeholder='student name'/>
                            </div> 
                            <div class='form-group'>
                                <input class='form-control student-field' type='text' id='studentContact-${i}' value='${_school.students[i].contact}' placeholder='student contact'/>
                            </div> 
                            <div class='form-group'>
                                <input class='form-control student-field' type='text' id='studentGrade-${i}' value='${_school.students[i].grade}' placeholder='student grade'/>
                            </div> 
                            <div class='form-group'>
                                <input class='form-control student-field' type='text' id='studentPaymentStatus-${i}' value='${_school.students[i].paymentStatus}' placeholder='payment status'/>
                            </div> 
                        </div>
                        <div class='modal-footer'>
                            <div class='btn-group'>
                                <button type='button' id='save-student-${i}' data-dismiss='modal' onclick='saveStudent(this)' class='btn btn-success'>save</button>
                                <button type='button' class='btn btn-danger' data-dismiss='modal'>close</button>
                            </div>
                        </div>
                    </div> 
                </div> 
            </div>`
                ;
        }

        _html += list_group_close;
    } else {
        _html = `<div class='alert warning'><p>no students in this list</p></div>`;
    }
    el_studentlist.html(_html);
}
// END modals
//add or remove items

// lesson plans
function addLessonPlan() {
    _school.lessonPlans.push(new lessonPlan($('#lessonPlanName').val()));
    saveSchool();
    populateLessonPlanList();
}
function saveLessonPlan(e) {
    var ind = $(e).attr('id').split('-')[2];
    _school.lessonPlans[ind].lessonName = $('#lessonPlanName-' + ind).val();
    saveSchool();
    populateLessonPlanList();
    $('.modal-backdrop').fadeOut();
    $(`#lesson-instance-${ind}`).click();
}
function deleteLessonPlan(e) {
    var ind = $(e).attr('id').split('-')[1];
    _school.lessonPlans.splice(ind, 1);
    saveSchool();
    populateLessonPlanList();
    $('.modal-backdrop').fadeOut();
    $(`#lesson-instance-${ind}`).click();
}
// lesson items
function addLessonItem(e) {
    var ind = $(e).attr('id').split('-')[1];
    _school.lessonPlans[ind].lessonItems.push(new lessonItem($('#lesson-item-name-' + ind).val(), $('#lesson-item-notes-' + ind).val()));
    saveSchool();
    populateLessonPlanList();

    $('.modal-backdrop').fadeOut();
    $(`#lesson-instance-${ind}`).click();
}
function saveLessonItem(e) {
    var splitid = $(e).attr('id').split('-');
    var lesson = splitid[1];
    var item = splitid[2];
    _school.lessonPlans[lesson].lessonItems[item].itemName = $('#lesson-item-name-' + item + "-" + lesson).val();
    _school.lessonPlans[lesson].lessonItems[item].itemNotes = $('#lesson-item-notes-' + item + "-" + lesson).val();
    saveSchool();
    populateLessonPlanList();
    $('.modal-backdrop').fadeOut();
    $(`#lesson-instance-${lesson}`).click();
}
function deleteLessonItem(e) {
    var splitid = $(e).attr('id').split('-');
    var lesson = splitid[1];
    var item = splitid[2];
    _school.lessonPlans[lesson].lessonItems.splice(item, 1);
    saveSchool();
    populateLessonPlanList();
    $('.modal-backdrop').fadeOut();
    $(`#lesson-instance-${lesson}`).click();

}

// syllabus adds
function addSyllabus() {
    _school.syllabii.push(new syllabus($('#syllabusName').val()));
    saveSchool();
    populateSyllabusList();
}
function deleteSyllabus(e) {
    var ind = $(e).attr('id').split('-')[1];
    _school.syllabii.splice(ind, 1);
    saveSchool();
    populateSyllabusList();
    $('.modal-backdrop').fadeOut();
    return false;
}
function saveSyllabus(e) {
    var ind = $(e).attr('id').split('-')[2];
    _school.syllabii[ind].syllabusName = $('#syllabusName-' + ind).val();
    saveSchool();
    populateSyllabusList();
    $('.modal-backdrop').fadeOut();
}
function deleteSyllabusItem(e) {
    var splitid = $(e).attr('id').split('-');
    var syllabus = splitid[1];
    var item = splitid[2];
    _school.syllabii[syllabus].syllabusItems.splice(item, 1);
    saveSchool();
    populateSyllabusList();
    $('.modal-backdrop').fadeOut();
    return false;
}
function addSyllabusItem(e) {
    var ind = $(e).attr('id').split('-')[1];
    _school.syllabii[ind].syllabusItems.push(new syllabusItem($('#syllabus-item-name-add-' + ind).val(), $('#syllabus-item-notes-add-' + ind).val()));
    saveSchool();
    populateSyllabusList();
    $('.modal-backdrop').fadeOut();
}
function saveSyllabusItem(e) {
    var splitid = $(e).attr('id').split('-');
    var syllabus = splitid[1];
    var item = splitid[2];
    _school.syllabii[syllabus].syllabusItems[item].title = $('#syllabus-item-name-' + item + "-" + syllabus).val();
    _school.syllabii[syllabus].syllabusItems[item].notes = $('#syllabus-item-notes-' + item + "-" + syllabus).val();
    saveSchool();
    populateSyllabusList();
    $('.modal-backdrop').fadeOut();
}

// add student
function addStudent() {
    _school.students.push(new student($('#studentName').val(), $('#studentContact').val()));
    saveSchool();
    populateStudentList();
}
function saveStudent(e) {
    var ind = $(e).attr('id').split('-')[2];
    _school.students[ind].studentName = $('#studentName-' + ind).val();
    _school.students[ind].contact = $('#studentContact-' + ind).val();
    _school.students[ind].grade = $('#studentGrade-' + ind).val();
    _school.students[ind].paymentStatus = $('#studentPaymentStatus-' + ind).val();
    saveSchool();
    populateStudentList();
    $('.modal-backdrop').fadeOut();
    $(`#${ind}`).click();
}
function deleteStudent(e) {
    var ind = $(e).attr('id').split('-')[1];
    _school.students.splice(ind, 1);
    saveSchool();
    populateStudentList();
    $('.modal-backdrop').fadeOut();
    return false;
}
// add class
function addClass() {
    _class.sessionDate = $('#classDate').val();
    _class.instructor = $('#classInstructor').val();
    _school.sessions.push(_class);
    saveSchool();
    alert(`${_class.sessionDate.toString()}-${_class.instructor} saved to your sessions.`);
    el_back[0].click();
    el_class_date.val('dd/mm/yyyy');
    el_class_instructor.val('');
    el_class_student_list.html('');
    el_class_lesson_list.html('');
    //enable lesson taught for next lesson
    el_lesson_taught.attr("disabled", false);
}
function saveClass(e) {
    var ind = $(e).attr('id').split('-')[1];
    _class.sessionDate = $('#').val();
    _class.instructor = $('#').val();
    _school.sessions[i] = _class;
    saveSchool();
}
function deleteClass(e) {
    var ind = $(e).attr('id').split('-')[1];
    _school.sessions.splice(ind, 1);
    saveSchool();
    populateClassesList();
    $('.modal-backdrop').fadeOut();
    return false;
}
function deleteClassStudent(e) {
    var splitid = $(e).attr('id').split('-');
    var session = splitid[1];
    var student = splitid[2];
    _school.sessions[session].classStudents.splice(student, 1);
    saveSchool();
    populateClassesList();
    $('.modal-backdrop').fadeOut();
    $(`#class-instance-${session}`).click();
    return false;
}

const loggedInFunctions = () => {
    $('body').click(() => {
        actionCount++;
        console.log(actionCount);
        if (actionCount > actionCountThreshold) {
            actionCount = 0;
            el_notification.fadeIn(500);
            setTimeout(() => { el_notification.fadeOut(500); }, 5000);
        }
    });
    $('#mainMenu > div').click(function () {
        if ($(this).attr("id") != "notamenuitem-0") {
            if (_school != undefined) {
                var i = $(this).attr('id').split('-')[1];
                el_mainmenu.addClass("hidden");
                $('#app-section-' + i).removeClass("hidden");
                mode = $('#app-section-' + i).find('h3.mode').text();
                switch (mode) {
                    case "roll call":
                        _class = new sessionInstance();
                        break;
                    case "students":
                        populateStudentList();
                        break;
                    case "syllabii":
                        populateSyllabusList();
                        break;
                    case "lesson plans":
                        populateLessonPlanList();
                        break;
                    case "classes":
                        populateClassesList();
                        break;
                    default:
                        break;
                }
            } else {
                alert("choose a school name first");
            }
        }
    });

    // edit schoolname
    el_schoolname.keyup(function (e) {
        if (e.which == '13') {
            el_schoolname.blur();
            if (localStorage.getItem('school') === null) {
                _school = new school(el_schoolname.text(), loggedInUser.userName);
                localStorage.setItem("school", JSON.stringify(_school));
            } else {
                _school.schoolName = el_schoolname.text();
                saveSchool();
            }
        }
    });
    // add a class listeners
    var stlist = [];
    var lslist = [];
    var lessonct = 0;

    el_lesson_taught.keyup(function (e) {
        //console.log("fired");
        if (e.which != '13') {
            var listHtml = "<div class='list-group glass-list-group'>";
            lslist = [];
            for (var i = 0; i < _school.lessonPlans.length; i++) {
                if (el_lesson_taught.val().length > 0 && _school.lessonPlans[i].lessonName.toLowerCase().includes(el_lesson_taught.val())) {
                    listHtml += `<a class='list-group-item lesson-plan-item' id='lessonplan-${i}'>${_school.lessonPlans[i].lessonName}</a>`;
                    lslist.push(_school.lessonPlans[i]);
                }
            }
            listHtml += "</div>";
            el_class_lesson_input_list.html(listHtml);
        } else {
            var listHtml = "";
            el_class_lesson_input_list.html(listHtml);
            $('#class-lesson-list').append(`<a class='list-group-item lesson-plan-item' id='lessonplan-item-${studentct}'>${lslist[0].lessonName}</a>`);
            _class.lessonPlan = lslist[0];
            lessonct++;
            el_lesson_taught.attr("disabled", true);
            el_lesson_taught.val("");
        }
    });
    var studentct = 0;
    el_add_class_student.keyup(function (e) {
        if (e.which != '13') {
            var listHtml = "<div class='list-group glass-list-group'>";
            stlist = [];
            for (var i = 0; i < _school.students.length; i++) {
                if (el_add_class_student.val().length > 0 && _school.students[i].studentName.toLowerCase().includes($('#addClassStudent').val())) {
                    listHtml += `<a class='list-group-item student-item' id='student-${i}'>${_school.students[i].studentName}</a>`;
                    stlist.push(_school.students[i]);
                }
            }
            listHtml += "</div>";
            $('#class-student-input-list').html(listHtml);
        } else {
            var listHtml = "";
            $('#class-student-input-list').html(listHtml);
            $('#class-student-list').append(`<a class='list-group-item student-item' id='student-${studentct}'>${stlist[0].studentName}</a>`);
            _class.classStudents.push(stlist[0]);
            el_add_class_student.val("");
            studentct++;
        }
    });
    // END add a class 
    el_logout.removeClass('hidden');

    // button events
    el_backup.click(function () {
        backupSchool();
    });
    el_load.click(function () {
        loadSchool();
    });
    el_logout.click(function () {
        logout(false);
    });
    el_back.click(function () {
        $('.app-section').addClass('hidden');
        $('#mainMenu').removeClass('hidden');
    });
    el_filter_classes.change(function () {
        let d = $('#filterClasses').val();
        console.log(d);
        let arr = [];
        for (var i = 0; i < _school.sessions.length; i++) {
            if (d.length > 0 && _school.sessions[i].sessionDate.toLowerCase().includes(d)) {
                arr.push(_school.sessions[i]);
            }
        }
        populateClassesList(arr);
    });

}
const checkReady = () => {
    var cr = setInterval(function () {
        if (readyState == true) {
            clearInterval(cr);
            readyState = false;

            if (_user != null && _user != undefined) {

                setLoggedInUser();
                el_username.html("welcome " + _user.userName);
                if (_user.paymentStatus == status_payment_trial) {
                    setTrialText(daysLeftInTrial(_user.lastPaid));
                }
                $('#login').addClass('hidden');
                $('#mainMenu').removeClass('hidden');
                $('#spinner').addClass('hidden');
                getSchool();
                loggedInFunctions();

            } else {
                $('#spinner').addClass('hidden');
                alert("no user found");
                checkReady();
            }
        }
    }, 50);
}
$(() => {
    if (storage) {

        if (partiallogin) {
            $('#uname').html("welcome " + loggedInUser.userName);
            if (loggedInUser.paymentStatus == status_payment_trial) {
                setTrialText(daysLeftInTrial(loggedInUser.lastPaid));
            }
            $('#mainMenu').removeClass('hidden');
            getSchool();
            loggedInFunctions();

        } else {
            //show full login
            $('#login').removeClass("hidden");
            $('#loginButton').click(function () {
                $('#spinner').removeClass('hidden');
                _user = authenticateUserWrapper($('#userName').val(), $('#password').val());
            });

            checkReady();

        }
    }
});